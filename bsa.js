import Chat from './src/components/Chat';
import rootReducer from './src/reducers/index';

export default {
  Chat,
  rootReducer
};