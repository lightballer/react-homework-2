import React from 'react';
import './Divider.css';

class Divider extends React.Component {
  render() {
    return (
      <div className="divider">
          <div className="line"></div>
          <div className="date">{ this.props.date }</div>
          <div className="line"></div>
        </div>
    );
  }
}

export default Divider;
