import React from 'react';
import { connect } from 'react-redux';
import './EditModal.css';
import { editMessage } from '../Chat/actions';
import { hideEditModal, dropCurrentId } from '../EditModal/actions';
import { getNewId } from '../../helpers/datesHelper';
import { setInputValue } from '../../helpers/inputHelper';

class EditModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    const id = nextProps.currentId;
    if (id) {
      const messages = this.props.messages;
      const text = messages.find(message => message.id === id).text;
      this.setState({
        text
      });
    }
  }

  updateMessageText(id, newText) {
    const editedAt = getNewId();
    this.props.editMessage(id, { 
      text: newText,
      editedAt
    });
  }

  onEditCancel() {
    this.props.hideEditModal();
    this.setState({
      text: '',
    })
  }

  onEditSubmit() {
    this.updateMessageText(this.props.currentId, this.state.text);
    this.props.hideEditModal();
    this.props.dropCurrentId();
    this.setState({
      text: '',
    });
  }

  getModalWindow(modalClass) {
    return (
      <div className="edit-modal-flex-container">
        <div className={ modalClass }>
          <header>Edit Message</header>
          <main className="input-container">
            <textarea 
              className="edit-message-input"
              value={ this.state.text }
              type="text" 
              placeholder="Text area" 
              cols="40" rows="5"
              onChange={ event => setInputValue.call(this, event, 'text') }/>
          </main>
          <section className="edit-buttons-container">
            <button className="edit-message-button" onClick={ this.onEditSubmit.bind(this) }>OK</button>
            <button className="edit-message-close" onClick={ this.onEditCancel.bind(this) }>Cancel</button>
          </section>
        </div>
      </div>
    );
  }

  render() {
    const modalClass = this.props.editModal ? "edit-message-modal modal-shown" : "edit-message-modal";
    return this.getModalWindow(modalClass);
  }
}

const mapStateToProps = state => ({
  messages: state.chat.messages,
  editModal: state.chat.editModal,
  currentId: state.chat.currentId,
});

const mapDispathToProps = {
  editMessage,
  hideEditModal,
  dropCurrentId,
};

export default connect(mapStateToProps, mapDispathToProps)(EditModal);
