import React from 'react';
import './Message.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart, faCog } from "@fortawesome/free-solid-svg-icons";
import { getTimeFromISO } from '../../helpers/datesHelper';

class Message extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLiked: false,
    };
  }

  onLikeClick() {
    this.setState({
      isLiked: !this.state.isLiked,
    });
  }

  onEdit() {
    const id = this.props.message.id;
    this.props.setCurrentId(id);
    this.props.showEditModal();
  }

  render() {
    const { avatar, user, text, createdAt } = this.props.message;
    return (
      <div className="message">
        <img className="message-user-avatar" src={ avatar } alt="avatar"/>    
        <div className="message-body">
          <div className="message-user-name">{ user }</div>
          <div className="message-text">{ text }</div>
        </div>
        <div className="message-metadata">
          <span className="message-time">{ getTimeFromISO(createdAt) }</span>
          <span className="edit-message" onClick={ this.onEdit.bind(this) }><FontAwesomeIcon icon={ faCog } /></span>
          <div 
            className={ this.state.isLiked ? 'message-liked' : 'message-like' } 
            onClick={ this.onLikeClick.bind(this) }>
              <FontAwesomeIcon icon={ faHeart } />
          </div>
        </div>
      </div>
    );
  }
}

export default Message;
