import React from 'react';
import { connect } from 'react-redux';
import './MessageInput.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleUp } from "@fortawesome/free-solid-svg-icons";
import { USER_ID } from '../../constants';
import { addMessage } from '../Chat/actions';
import { getNewId } from '../../helpers/datesHelper';
import { setInputValue } from '../../helpers/inputHelper';

class MessageInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  createMessage(createdAt, text) {
    const newMessage = {
      id: createdAt,
      userId: USER_ID,
      text,
      createdAt,
    };
    this.props.addMessage(newMessage.id, newMessage);
  }

  onSendMessage() {
    const id = getNewId();
    this.createMessage(id, this.state.text);
    this.setState({
      text: '',
    });
  }

  onEnterPressed(event) {
    const key = event.key;
    if (key === 'Enter') {
      this.onSendMessage();
    }
  }

  render() {
    return (
      <div className="message-input">
        <input 
          type="text"
          value={this.state.text}
          className="message-input-text" 
          placeholder="Message"
          onChange={ event => setInputValue.call(this, event, 'text') }
          onKeyPress= { this.onEnterPressed.bind(this) }/>
        <button 
          className="message-input-button" 
          onClick={ this.onSendMessage.bind(this) }>
            <span className="send-icon"><FontAwesomeIcon icon={ faArrowAltCircleUp }/></span> 
            Send</button>
      </div>
    );
  }
}

const mapStateToProps = state => ({

});

const mapDispathToProps = {
  addMessage,
};
  
export default connect(mapStateToProps, mapDispathToProps)(MessageInput);