import React from 'react';
import './Header.css';
import { getDateAndTimeFromISO } from '../../helpers/datesHelper';

class Header extends React.Component {
  render() {
    const { participantsCount, messagesCount, lastMessageTime } = this.props;
    return (
      <header className="header">
        <p className="header-title">Chat Name</p>
        <p className="header-users-count">{ participantsCount } participants</p>
        <p className="header-messages-count">{ messagesCount } messages</p>
        <p className="header-last-message-date">last message: { lastMessageTime ? getDateAndTimeFromISO(lastMessageTime) : '' }</p>
      </header>
    );
  }
}

export default Header;
